#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

using namespace std;

int idx(int i, int j, int y);
void printmap(bool map[], int x, int y, int t);
void deletemap(int x, int y);
string repeat(int n, string s);
bool* next(bool map[], int x, int y);
int countn(bool map[], int x, int y, int xp, int yp);

int main(int argc, char *argv[]) {
  if(argc != 2) {
    cout << "Please specify config file." << endl;
    return 1;
  }

  char* filepath = argv[1];

  std::ifstream infile(filepath);

  int x, y, psize;

  infile >> x >> y;

  infile >> psize;

  bool* map = new bool[x * y];

  for(int i = 0; i < x; i++) {
    for(int j = 0; j < y; j++) {
      map[idx(i, j, y)] = false;
    }
  }

  while(psize > 0) {
    int px, py;
    infile >> px >> py;

    map[idx(px, py, y)] = true;
    psize--;
  }

  int tmax;
  infile >> tmax;


  for(int t = 0; t < tmax; t++) {
    printmap(map, x, y, t);
    usleep(300000);
    map = next(map, x, y);
    deletemap(x, y);
  }

  printmap(map, x, y, tmax);


  return 0;
}

int idx(int i, int j, int y) {
  return i * y + j;
}

/**
 * Methods for printing out the map
 */
void printmap(bool map[], int x, int y, int t) {
  string dashline = repeat(6 * y + 6, "-");

  cout << dashline << endl;

  printf("%4d |", t); // print current round
  for(int j = 0; j < y; j++) {
    printf(" %3d |", j);
  }

  cout << endl << dashline << endl;

  for(int i = 0; i < x; i++) {
    printf(" %3d |", i);
    for(int j = 0; j < y; j++) {
      if(map[idx(i, j, y)])
        cout << "  *  |";
      else
        cout << "     |";
    }
    cout << endl << dashline << endl;
  }
}

void deletemap(int x, int y) {
  string delmap = repeat(x * 2 + 3, "\r\e[A");
  cout << delmap;
}

string repeat(int n, string s) {
  ostringstream os;
  for(int i = 0; i < n; i++) {
    os << s;
  }
  return os.str();
}

bool* next(bool map[], int x, int y) {
  bool* nmap = new bool[x * y];
  for(int i = 0; i < x; i++) {
    for(int j = 0; j < y; j++) {
      int nc = countn(map, x, y, i, j);
      if(map[idx(i, j, y)]) {
        if(nc == 2 || nc == 3) {
          nmap[idx(i, j, y)] = true;
        } else {
          nmap[idx(i, j, y)] = false;
        }
      } else {
        if(nc == 3) {
          nmap[idx(i, j, y)] = true;
        } else {
          nmap[idx(i, j, y)] = false;
        }
      }
    }
  }
  return nmap;
}

int countn(bool map[], int x, int y, int xp, int yp) {
  int nc = 0;

  for(int i = xp - 1; i <= xp + 1; i++) {
    for(int j = yp - 1; j <= yp + 1; j++) {
      if( (i == xp && j == yp) || 
          i < 0 || i >= x || 
          j < 0 || j >= y ) {
        continue;
      }

      if(map[idx(i, j, y)])
        nc += 1;
    }
  }

  return nc;
}
